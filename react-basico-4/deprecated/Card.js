import React from 'react';
import styled from 'styled-components';

import waves from './images/waves.svg';

const Container = styled.div`
    position:relative;
    height:150px;
    width:350px;
    border-radius: 25px;
    background-color: #ffefea;
    display:flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    z-index: 0;
`;

const Title = styled.p`
    width: 65%;
    color: #ff8b60;
    font-family: "Nunito";
    font-style: italic;
    font-weight: bold;
    text-align: center;
    font-size: 0.8rem;
    text-transform: uppercase;
`

const Button = styled.button`
    color:white;
    background-color: #ff9a77;
    font-family: "Nunito";
    font-style: italic;
    font-weight: bold;
    border-style: none;
    border-radius: 50px;
    padding: 0.3rem 0.8rem;
    font-size: 0.75rem;
    text-transform: uppercase;
    cursor: pointer;
    &:hover {
        background-color: #fd8053;
    }
`;

const Wave = styled.div`
    position:absolute;
    aspect-ratio: 350/150;
    width:100%;
    background-repeat: no-repeat;
    background-position: bottom;
    background-size: cover;
    background-image: url(${props => props.backgroundImage});
    z-index: -1;
    border-radius: 0 0 25px 25px;
`

const App = ()=>{
    return(
        <Container>
            <Title>use code: pigi100 for rs.100 off on your first order!</Title>
            <Button>grab now</Button>
            <Wave backgroundImage = {waves}>
            </Wave>
        </Container>
    );
}

export default App;