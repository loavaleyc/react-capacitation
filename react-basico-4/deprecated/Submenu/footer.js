/* eslint-disable default-case */
import React from 'react';
import styled, { ThemeProvider } from 'styled-components';

import Check from '../images/check.png';
import Cross from '../images/close.png';
import Reset from '../images/reset.png'
import {Circle, Icon, theme} from './options';


const Container = styled.div`
    position:${props => props.theme.position};
    display: flex;
    flex-direction: row;
    transform: scale(2);
    right:140px;
    justify-content: ${props => props.theme.justifyContent};
    margin:  ${props => props.theme.margin};
`;

Container.defaultProps = {
    theme:{
        position: 'absolute', 
        justifyContent: 'center',
        margin: '0'
    }
}

const tema = {
    position: 'static', 
    justifyContent: 'center',
    margin: '25px 2px'
};

const Divisor = styled.hr`
    border: 0.05rem solid #ffffff73;
    margin:0;
`
const Item =({v, onClick}) =>{
    return (
        <Circle visible={true} onClick={onClick}>
                     <Icon visible={true} src={v}></Icon>
                </Circle>
    )
}

const App = ({version, onReset, onCancel, onAcept}) =>{
    

    const handleReset =()=>{
        console.log("vamos a resetear")

        if(typeof onReset === 'function') onReset();
    }

    const handleOk =()=>{
        console.log("vamos a aceptar")
        if(typeof onAcept === 'function') onAcept();
    }

    const handleCancel=()=>{
        console.log("vamos a cancelar")
        if(typeof onCancel === 'function') onCancel();
    }

    
    /**
     *  color: string;
     *  options: string[];
     */
     if(version===1){
        let data = [
            {icon: Cross, onClick:handleCancel},
            {icon: Check, onClick:handleOk}
        ]
        return( <Container>

        
            {data.map((v,i)=>{
                return (
                    <ThemeProvider theme={theme}>
                        <Item v={v.icon} onClick={v.onClick} ></Item>
                    </ThemeProvider>
                )
                })}
        
        </Container>)
     }

     if(version===2){
        let data = [
            {icon: Cross, onClick:handleCancel}, 
            {icon: Reset, onClick:handleReset},
            {icon: Check, onClick:handleOk}
        ]
        return( <ThemeProvider theme={tema}>
            <Divisor></Divisor>
            <Container>
            
            {data.map((v,i)=>{
                return (
                    <Item v={v.icon} onClick={v.onClick}></Item>
                )
                })}
        
        </Container></ThemeProvider>)
     }
}

export default App;