import React, {useState} from 'react';
import styled from 'styled-components';
import Card from './Card.js';

import Submenu from './Submenu';

const Container = styled.div`
`;

function App() {

  const [data, setData] = useState([
      {
        id: 1,
        color: "#21d0d0", options: [
          { title: "price low to high", state: false },
          { title: "high to low", state: false },
          { title: "popularity", state: true },
        ], footer: 1
      },
      {
        id:2,
        color: "#ff7745", options: [
          { title: "1up nutrition", state: false },
          { title: "asitis", state: false },
          { title: "avvatar", state: false },
          { title: "big muscles", state: false },
          { title: "bpi sports", state: false },
          { title: "bsn", state: false },
          { title: "cellucor", state: false },
          { title: "domin8r", state: false },
          { title: "dymatize", state: false },
        ], footer: 2
      },]
  )


  const handleChange = (value,id) =>{

      const match = data.filter(el =>el.id === id)[0];
    	
      const act = match.options.filter(el => el.title === value)[0];
      
      const copy = [...match.options];

      const targetIndex = copy.findIndex(f => f.title === value);
      if(targetIndex > -1){
        copy[targetIndex] = {title: value, state: !act.state};

        match.options = copy;
      };

      const copyList = [...data];

      const targetIndexList = copyList.findIndex(f => f.title === value);

      if(targetIndexList > -1){
        copy[targetIndex] = match;
      }

      setData(copyList);
    }

    const handleReset = (id) =>{
      const copy = [...data];

      const targetIndex = copy.findIndex(f => f.id === id);

      if(targetIndex > -1){
        let raw =[];
        const options = data.filter(el => el.id === id)[0].options;
        for(let o of options){
          raw.push({title: o.title, state:false});
          
        }
        copy[targetIndex].options = raw;
        
      }

      setData(copy);
    }

  return (
    <Container>
      <Card></Card>

      {data.map((v, i) => <Submenu {...v} onChange={handleChange} onReset={handleReset}></Submenu>)}
    </Container>
  );
}
export default App;