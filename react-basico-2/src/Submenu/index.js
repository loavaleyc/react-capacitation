import React from 'react';
import styled from 'styled-components';

import Options from './options';
import Footer from './footer';

const Container = styled.div`
    width:350px;
    background-color: ${props => props.color};
    border-radius: 25px;
    position:relative;
    padding: 10px 0px;
    margin: 15px 0 45px 0;
`;



const App = (params) =>{
    
    /**
     *  color: string;
     *  options: string[];
     */

    const handleChange =(value) =>{
        if(typeof params.onChange === 'function') params.onChange(value, params.id);
    }

    const handleReset =() =>{
        if(typeof params.onReset === 'function') params.onReset(params.id);
    }
    return(
        <Container color={params.color}>
            {params.options.map((v, i) => <Options key={i}{...v} onChange={handleChange}></Options>)}
            <Footer version={params.footer} onReset={handleReset} onCancel onAcept>
        </Footer>
        </Container>
        
        
    );
}

export default App;