import React from "react";
import styled from "styled-components";

import Check from '../images/check1.png';

const Container = styled.div`
  padding: 2px 30px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  position: relative;
`;

const Title = styled.p`
  color: white;
  text-transform: uppercase;
  font-family: "Nunito";
  font-weight: bold;
  font-size: 0.95rem;
`;

export const Circle = styled.div`
    width: 16px;
    height: 16px;
    padding:4px;
    margin: 2px;
    border: 0.10rem solid white;
    border-radius: 50px;
    cursor:pointer;
    background-color: ${props => props.visible ? 'white': 'transparent'};
    box-shadow: ${props => props.theme.main};
`;

Circle.defaultProps = {
    theme: {
        main: "0 0 0 0"
    }
}

export const theme = {
    main: "0 0.2rem 0.3rem  #8b8a8a38",
};

export const Icon = styled.img`
    width: 16px;
    height: 16px;
    margin:auto;
    visibility:  ${props => props.visible ? 'visible': 'hidden'};
`


const App = ({ title, state, onChange }) => {


    const handleChange = ()=>{
      if(typeof onChange === 'function') onChange(title);
    }
  return (
    <Container>
      <Title>{title}</Title>
      <Circle visible={state} onClick={handleChange}>
        <Icon visible={state} src={Check}></Icon>
      </Circle>
    </Container>
  );
};

export default App;
