import React from 'react';
import styled from 'styled-components';



const Container = styled.div`
   width: 20%;
`
const Body = styled.div`
    background-color:${props => props.colorIcon};
    height:100%;
    width:100%;
    border-radius: 25px;
    display:flex;
    justify-content: center;
    align-items: center;
`

const Icon = styled.img`

`

const App = ({ico, colorIcon}) =>{
  return (
    <Container>
        <Body colorIcon = {colorIcon}>
         
          <Icon  src={ico}></Icon>
        </Body>
       
    </Container>
  );
}

export default App;
