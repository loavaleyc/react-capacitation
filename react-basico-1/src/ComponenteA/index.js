import React from 'react';
import styled from 'styled-components';

import Icon from './icon';
import Body from './body';

const Container = styled.div`
    height: 70px;
    width: 350px;
    display:flex;
    flex-direction: row;
    margin-top: 25px;
`

const App = (params) =>{
  return (
    <Container>
        <Icon ico ={params.ico} colorIcon={params.colorIcon}></Icon>
        <Body title={params.title} description={params.description} link={params.link}></Body>
    </Container>
  );
}

export default App;
