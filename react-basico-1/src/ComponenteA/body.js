import React from 'react';
import styled from 'styled-components';


const Container = styled.div`
    width:70%;
    padding-left: 25px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`

const TitleContainer = styled.div`
    display: flex;
    flex-direction: row;
`

const Title = styled.p`
    margin:0px;
    font-size: 0.94rem;
    font-weight: 500;
    color: #767980;
`
const Description  = styled.p`
    margin:0px;
    padding: 5px 0;
    color: #abb0b1;
    font-size: 0.88rem;
    
`
const Link = styled.a`
    color: #3cb0c7;
    font-weight: 500;
    font-size: 0.88rem;
`

const Status = styled.div`
    background-color: #e6e4e4;
    border-radius: 2px;
    padding: 0.2rem 0.3rem;
    font-size: 0.7rem;
    margin: 0 5px;
    color: #7c7c7c;
`

const App = (params) =>{
  return (
    <Container>
        <TitleContainer>
        <Title>{params.title}</Title>
        <Status>Desactivado</Status>
        </TitleContainer>
        
        <Description>{params.description}</Description>
        <Link>{params.link}</Link>
    </Container>
  );
}

export default App;
