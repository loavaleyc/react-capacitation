import React, {useState, useEffect} from 'react';
import styled, { css } from 'styled-components';

const Container = styled.div`
    display:flex;
    flex-direction: row;
`
const Link = styled.a`
    font-weight: 500;
    color: #767980;
    padding: 10px;
    cursor: pointer;
    &.active{
        
        ${props =>
    props.select &&
    css`
      color: cornflowerblue;
    `}
        text-underline-offset: ${props => props.select && '0.5em' };
    }
`;


const App = ({index, link, state})=>{

    let [select, setSelect] = useState(false);
    

    console.log({index});
    console.log({link});
    useEffect(()=>{
        if(state) setSelect(true);
        else setSelect(false);
    },[state]);

    const handleChange = (e)=>{
        setSelect(!select);
            let el = document.getElementsByTagName('a');
            for(const element of el){
            element.classList.remove('active');
        }
    }
    return (
        <Container>
            <Link className="active" select={select} onClick={handleChange}>{link}</Link>
        </Container>
    );
};

export default App;