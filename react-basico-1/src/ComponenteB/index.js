import React from 'react';
import styled from 'styled-components';

import Nav from './nav';
import Body from './body';

const Container = styled.div`
    width: 600px;
    height: 600px;
    display:flex;
    flex-direction: column;
`
const NavBar = styled.div`
    display: flex;
    flex-direction: row;
`;

const App = ()=>{

    const links = [
        {link: 'Resumen', state: true},
        {link: 'Administración', state: false},
        {link: 'Configuración de tienda online', state: false}
    ]
    return (
        <Container>
            <NavBar>{links.map((v, i) => { console.log(i); return <Nav key={i}{...v} index={i}></Nav>})}</NavBar>
            <Body></Body>
        </Container>
    );
};

export default App;