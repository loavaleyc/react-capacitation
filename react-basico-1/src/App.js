import React from 'react';
import styled from 'styled-components';


import ComponenteA from './ComponenteA';
import ComponenteB from './ComponenteB';
import crown from './images/crown.png';
import browser from './images/browser.png';
import envelope from './images/envelope.png';

const Container = styled.div`   
  display: flex;
  flex-direction: row;
`;

const Main = styled.main`
  display: flex;
  flex-direction: column;
`;

const Aside = styled.aside`
`;


const App = () =>{
  return (
    <Container>
      <Main>
          <ComponenteB></ComponenteB>
      </Main>
      <Aside>
        <ComponenteA ico={crown} colorIcon={"#fff5e7"} title={"Paquete premium"} description={"Descubre nuevas funciones"} link={"Hazte Premium"}>
        </ComponenteA>
        <ComponenteA ico={browser} colorIcon={"#e9fbf1"} title={"Dominio"} description={"Registra tu propio dominio, p. ej. www.mipaginaweb.com"} link={"Registrar un nuevo dominio"}>
        </ComponenteA>
        <ComponenteA ico={envelope} colorIcon={"#f2f1f6"} title={"Cuentas de email"} description={"Envia correos profesionales"} link={"Crear nueva cuenta de email"}>
        </ComponenteA>
      </Aside>
      
    </Container>
  );
}

export default App;
